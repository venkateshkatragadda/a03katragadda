var express = require('express');  
var app = express();  
var server = require('http').createServer(app);  
var io = require('socket.io')(server); 
var path = require('path');

var logger = require("morgan");
var bodyParser = require("body-parser");

// set up the view engine
app.set("views", path.resolve(__dirname, "views")); 
app.set("view engine", "ejs"); 

app.use(express.static(__dirname+'/assets'));
// manage our entries
var entries = [];
app.locals.entries = entries;

// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
 
// on a GET request to default page, do this.... 
app.get('/', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Katragadda/assets', 'Home.html'));
});
app.get('/Aboutme', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Katragadda/assets', 'Aboutme.html'));
});
app.get('/order', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Katragadda/assets', 'order.html'));
});
app.get('/Contact', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Katragadda/assets', 'Contact.html'));
});
app.get('/Home', function(req, res){
  app.use(express.static(path.join(__dirname)));
  res.sendFile(path.join(__dirname, '../A03Katragadda/assets', 'Home.html'));
});
app.get("/guestbook", function (request, response) {
  response.render("guestbook");
});
app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});
 app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");
});
// Listen for an app request on port 8081
server.listen(8081, function(){
  console.log('listening on http://127.0.0.1:8081/');
});