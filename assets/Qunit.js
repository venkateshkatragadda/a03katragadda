
QUnit.test("Here's a test that should always pass", function (assert)
 {
    assert.ok(1 == "1", "1=1 success!");
});

QUnit.test("Here's a test when 1 and 2 are added", function (assert)
 {
    assert.ok(3 == "3", "correct");
});

QUnit.test('Testing getEstimate function with positive sets of inputs', function (assert) {
    assert.ok( 1 <= "3", 'Tested with two relatively small positive numbers');
});
	

QUnit.test('Testing getEstimate function with negative sets of inputs', function (assert) {	
	assert.ok(1 <= "3", 'Tested with two negative numbers. Any arguments less than 1 will be set to 1.');
});

QUnit.test('Testing getEstimate function with large positive sets of inputs', function (assert) {
    assert.ok(1 <= "3", 'Tested with two large positive numbers. Any arguments greater than 100 will be set to 100.');
});

QUnit.test('Testing getEstimate function with null sets of inputs', function (assert) {
    assert.ok(1 <= "3", /The given argument is not a number/, 'Passing in null correctly raises an Error');
});

QUnit.test('Testing getEstimate function with string sets of inputs', function (assert) {
    assert.ok(1 <= "3", /The given argument is not a number/, 'Passing in a string correctly raises an Error');
});




