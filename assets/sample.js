 var getSum = function (arg1, arg2) {
         var intArg1 = parseInt(arg1);
         var intArg2 = parseInt(arg2);
         return intArg1 + intArg2;
     };
      
     var getSumText = function (arg1, arg2) {
         var sum = getSum(arg1, arg2);
         return 'The sum of ' + arg1 + ' and ' + arg2 + ' is ' + sum + '.';
   };
     
    $("#button1").click(function (e) {
        var sumText = getSumText($("#arg1").val(), $("#arg2").val());
        $("#output1").text(sumText);
        e.stopPropagation();