# A03 Assignment

A simple WebApllication. 

# Uses

- HTML, a markup language for web apps
- CSS, a style sheet language for styling markup languages like html
- JavaScript, a scripting language for the web
- BootStrap, a framework for responsive web design
- QUnit, a unit testing framework for JavaScript
-Node.js platform
-Express web framework
-BootStrap framework

## How to Use 
Open a command window in your c:\44563\A03 folder.

Run node server.js to start the server.  

```
> nodemon  server.js
```

Point your browser to `http://localhost:8081`. 


# References

- https://qunitjs.com/
- https://www.sitepoint.com/getting-started-qunit/
- http://jsbin.com/tusuvixi/1/edit?html,js,output
- http://dillinger.io/
- https://validator.w3.org/
